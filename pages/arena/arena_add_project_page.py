from selenium.webdriver.common.by import By


class ArenaAddProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def click_add_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.button_link').click()

    def input_project_data(self, title, prefix, description):
        self.browser.find_element(By.ID, 'name').send_keys(title)
        self.browser.find_element(By.ID, 'prefix').send_keys(prefix)
        self.browser.find_element(By.ID, 'description').send_keys(description)
        self.browser.find_element(By.ID, 'save').click()
