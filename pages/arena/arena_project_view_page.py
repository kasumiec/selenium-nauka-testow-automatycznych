from selenium.webdriver.common.by import By


class ArenaProjectViewPage:

    def __init__(self, browser):
        self.browser = browser

    def click_project_page(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.activeMenu').click()

