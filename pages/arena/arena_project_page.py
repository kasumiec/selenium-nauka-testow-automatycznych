from selenium.webdriver.common.by import By


class ArenaProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title

    def click_add_project(self):
        self.browser.find_element(By.CSS_SELECTOR, 'a.button_link').click()

    def search_project_by_name(self, project_title):
        search_input = self.browser.find_element(By.ID, 'search')
        search_button = self.browser.find_element(By.ID, 'j_searchButton')
        search_input.send_keys(project_title)
        search_button.click()

    def get_first_result(self):
        results = self.browser.find_element(By.CSS_SELECTOR, 'tbody')
        first_result = results.find_element(By.XPATH, './tr/td/a')
        return first_result
