import pytest
from selenium.webdriver import Chrome

from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login import ArenaLoginPage
from pages.arena.arena_project_page import ArenaProjectPage
from utils.random_message import generate_random_text
from pages.arena.arena_add_project_page import ArenaAddProjectPage
from pages.arena.arena_project_view_page import ArenaProjectViewPage


@pytest.fixture
def browser():
    driver = Chrome()
    driver.get('http://demo.testarena.pl/zaloguj')
    arena_login_page = ArenaLoginPage(driver)
    arena_login_page.login('administrator@testarena.pl', 'sumXQQ72$L')
    yield driver
    driver.quit()


def test_project_creation_flow(browser):
    arena_home_page = ArenaHomePage(browser)

    # otworzenie admin panel
    arena_home_page.click_tools_icon()
    arena_project_page = ArenaProjectPage(browser)
    arena_project_page.verify_title('Projekty')

    # dodanie nowego projektu generując losowe dane
    arena_project_page.click_add_project()

    title = generate_random_text(10)
    prefix = generate_random_text(6)
    description = generate_random_text(13)

    arena_add_project_page = ArenaAddProjectPage(browser)
    arena_add_project_page.input_project_data(title, prefix, description)

    # wejście do sekcji projects
    arena_project_view_page = ArenaProjectViewPage(browser)
    arena_project_view_page.click_project_page()

    # wyszukanie nowo utworzonego projektu po nazwie
    arena_project_page.search_project_by_name(title)
    first_result = arena_project_page.get_first_result()
    assert first_result.text == title
