import time

from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_duckduckgo():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome()

    # Otwarcie strony duckduckgo
    browser.get('http://duckduckgo.pl')
    browser.set_window_size(1920, 1080)
    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.CSS_SELECTOR, '#search_form_input_homepage')

    # Znalezienie guzika wyszukiwania (lupki)
    search_button = browser.find_element(By.CSS_SELECTOR, '#search_button_homepage')

    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed() is True
    assert search_button.is_displayed() is True

    # Szukanie 4_testers
    search_input.send_keys('4testers')
    search_button.click()

    # Sprawdzenie że jakikolwiek wynik ma tytuł '4testers'
    # lista seleniumowych WEBelementów, ktore reprezentują nam tytuły stron
    results = browser.find_element(By.CSS_SELECTOR, 'h2 a')

    # lista tytułów
    list_of_titles = []
    for i in results:
        list_of_titles.append(i.text)

    assert '4_testers - kurs dla testerów oprogramowania' in list_of_titles

    # Zamknięcie przeglądarki

    time.sleep(3)
    browser.quit()


